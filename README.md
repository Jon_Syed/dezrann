
# Dezrann


**Dezrann** is an open-source platform to *study, hear, and annotate music on the web*.
Dezrann usages include *music education*, as well as corpus annotation for *musicology or computer music research*.

The public beta server is available from <http://www.dezrann.net>.

The code is hosted on <https://gitlab.com/algomus.fr/dezrann>.
The code is licensed under GPL v3+.

See [dez-about](https://gitlab.com/algomus.fr/dezrann/-/blob/dev/code/dez-client/dezrann/src/dezrann-app/dez-about.html) for our roadmap.

## Installation and running instructions

See <docs/dev/install.md>.

## Contact

Dezrann is developed by Emmanuel Leguy and the [Algomus](http://www.algomus.fr) team.
Contact us at <contact@dezrann.net>.

## References

- Mathieu Giraud, Richard Groult, Emmanuel Leguy,
  [Dezrann, a Web Framework to Share Music Analysis](https://hal.archives-ouvertes.fr/hal-01796787v1),
  Int. Conf. on Technologies for Music Notation and Representation ([TENOR 2018](http://matralab.hexagram.ca/tenor2018/)), 2018

- Ming La, Mathieu Giraud, Emmanuel Leguy,
  [Realtime collaborative annotation of music scores with Dezrann](https://hal.archives-ouvertes.fr/hal-02162935),
  CMMR 2019.

- 🇫🇷 [Favoriser l'écoute autonome en éducation musicale](https://www.ac-amiens.fr/2512-favoriser-l-ecoute-autonome-en-education-musicale.html),
  Académie d'Amiens, 2019.
