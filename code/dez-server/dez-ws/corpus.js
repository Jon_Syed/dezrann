let fs = require('fs');
const PieceDataFormater = require('./lib/PieceDataFormater');
const AjvJsonFormatValidator = require('./lib/AjvJsonFormatValidator');

let pieceDataFormater = new PieceDataFormater(new AjvJsonFormatValidator())

let config = ""
if (fs.existsSync('config.json')) {
  config = JSON.parse(fs.readFileSync('config.json', 'utf8'))
} else {
  console.log("Config file does not exist!");
  process.exit()
}

function canAccess (access, principal, permission) {
  if (access[permission] == undefined) return false
  return access[permission].includes('public')
    || access[permission].includes(principal.uid)
    || principal.groups.filter(g => access[permission].includes(g)).length > 0
}

function can (verb, principal, path, filename = "") {
  if (path == "") return false
  if (principal.groups.includes("admin")) return true
  try {
    let access = JSON.parse(fs.readFileSync(path + '/access.json', 'utf8'))
    if (filename != "" && access[filename]) {
      return canAccess(access[filename], principal, verb)
    } else if (access[verb]) {
      return canAccess(access, principal, verb)
    } else {
      return can(verb, principal, parentPath(path))
    }
  } catch (e) {
    if (e.code == "ENOENT") {
      return can(verb, principal, parentPath(path))
    } else {
      throw e
    }
  }
}

function parentPath (path) {
  let split = path.split('/')
  let parentLength = split.length - 1
  if (split[split.length - 1] == "") {
    parentLength = split.length - 2
  }
  return split.slice(0, parentLength).join("/")
}

function formatePath (path) {
  return path?path.slice(1) + "/":""
}

function respond (response, data, err, raw) {
    if (err) {
        if (err.code === 'ENOENT' || err === 'NOT_FOUND') {
            response.writeHead(404, "FILE NOT FOUND! -> " + err.message);
        } else if (err == "FORBIDDEN") {
          response.writeHead(401, "access forbidden!")
        } else {
            response.writeHead(500, err.message);
        }
        response.end();
    } else {
        response.writeHead(
            200,
            {
                'Content-Type': 'text/json; charset=utf-8',
                'Access-Control-Allow-Origin': '*'
            }
        );
        response.end((typeof raw === 'undefined' || !raw) ? JSON.stringify(data, null, 2) : data);
    }
}

function printlog(request, msg) {
  let params = ""
  for (const key in request.params) {
    params += request.params[key] + "/"
  }
  let message = (new Date()) + " - " + request.user.uid + " - " + msg + " (" + params + ")"
  fs.appendFile('dezws.log', message + "\n", function (err) {
    if (err) throw err;
  });
}

class Corpus {

  static _getAccessFileContent (path) {
    if (path == "") return []
    try {
      return JSON.parse(fs.readFileSync(path + '/access.json', 'utf8'))
    } catch (e) {
      if (e.code = "ENOENT") {
        return Corpus._getAccessFileContent(parentPath(path))
      }
    }
  }

  static _getAccessOf (path, filename = "") {
    let access = Corpus._getAccessFileContent(path)
    if (filename != "") {
      if (access[filename]) {
        access = access[filename]
      } else {
        access = Corpus._getAccessFileContent(parentPath(path))
      }
    }
    return Object.keys(access).map(key => {
      return {
        verb : key,
        subjects : access[key]
      }
    })
  }

  static _hasAccess(principal, access) {
    return access.subjects.includes("public")
      || access.subjects.includes(principal.uid)
      || principal.groups.filter(g => access.subjects.includes(g)).length > 0
  }

  static _getPermissions (principal, path) {
    if (principal.groups.includes("admin")) return ["all"]
    let accesses = Corpus._getAccessOf(path)
    let ret = []
    accesses.forEach (access => {
      if (Corpus._hasAccess(principal, access)) {
        ret.push(access.verb)
      }
    })
    return ret
  }

  static _mutatePath (path, newPath) {
    return (path && path.startsWith('http'))?path:newPath
  }

  static _setImagesPaths (images, root) {
    return images.map(image => {
      let re = /(.*)(.png|.jpg)$/
      let match = re.exec(image.image)
      let prefix = root + match[1]
      image.positions = Corpus._mutatePath(image.positions, prefix + "/positions")
      image.image = Corpus._mutatePath(image.image, prefix + "/file")
      return image
    })
  }

  static _getPieceData (path, id, principal) {
    if (!can('read', principal, path + id)) throw "FORBIDDEN"
    let piece = JSON.parse(fs.readFileSync(path + id + '/info.json', 'utf8'))
    pieceDataFormater.formate(piece)
    if (can('read-access', principal, path + id)) {
      piece["access"] = Corpus._getAccessOf(path + id)
    }
    piece["permissions"] = Corpus._getPermissions(principal, path + id)
    return piece
  }

  static _getCorpusData (path, principal, recursive = false) {
    if (!can('read', principal, path)) return ""
    let dirs = fs.readdirSync(path)
    let pieces = []
    let corpora = []
    for (let dir of dirs) {
      if (dir == "access.json") continue
      try {
        let piece = Corpus._getPieceData(path, dir, principal)
        if (!piece.hide) pieces.push(piece)
      } catch (e) {
        try {
          if (can('read', principal, path + dir)) {
            if (recursive) {
              corpora.push(Corpus._getCorpusData(path + dir + "/", principal, recursive))
            } else {
              let corpus = {
                name : dir
              }
              if (can('read-access', principal, path + dir)) {
                corpus["access"] = Corpus._getAccessOf(path + dir)
              }
              corpora.push(corpus)
            }
          } else {
            throw "FORBIDDEN"
          }
        } catch (e) {
          if (e.code == "ENOTDIR") {
          } else if (e == "FORBIDDEN") {
          } else {
            throw e
          }
        }
      }
    }
    let ret = { id: path.split("/").reverse()[1] }
    if (pieces.length > 0) ret["pieces"] = pieces
    if (corpora.length > 0) ret["corpora"] = corpora
    return ret
  }

  static corpus(path, id, user, sendData, recursive) {
    let formatedPath = formatePath(path)
    let principal = user?user:{"uid": "public", "groups": []}
    try {
      sendData(undefined, Corpus._getPieceData(formatedPath, id, principal))
    } catch (e) {
      if (e.code == "ENOENT") {
        try {
          sendData(undefined, Corpus._getCorpusData(formatedPath + id + "/", principal, recursive))
        } catch (e) {
          sendData(e, undefined)
        }
      } else {
        sendData(e, undefined)
      }
    }
  }

  static recursive (req, res, next) {
    printlog(req, "get piece or corpus recursive");
    Corpus.corpus(req.params[0], req.params.id, req.user, (err, data) => {
      respond(res, data, err)
    }, true)
  }

  static flat (req, res, next) {
    printlog(req, "get piece or corpus");
    Corpus.corpus(req.params[0], req.params.id, req.user, (err, data) => {
      respond(res, data, err)
    })
  }

  static _positions(path, id, user, possiblePaths, sendData) {
    let root = formatePath(path) + id
    // if (!can('read', user, root)) {
    //   sendData(undefined, "")
    //   return
    // }
    for (const filepath of possiblePaths) {
      let fp = root + filepath
      if (fs.existsSync(fp)) {
        fs.readFile(fp, 'utf8', (err, data) => {
          if (err) {
            sendData(err, data)
          } else {
            try {
              sendData(err, JSON.parse(data))
            } catch (e) {
              sendData(e, data)
            }
          }
        })
        return
      }
    }
    sendData("NOT_FOUND", "")
  }

  static _imagePositions(params, user, sendData) {
    let possiblePaths = [
      '/sources/images/' + params.imageName + '/positions.json', // nominal
      '/positions' + params.imageName.split(params.imageName)[1] + '.json', // ex positions-autograph.json positions.json
      '/' + params.imageName + '.json', // ex: stand-by-me-lyrics.json
    ]
    Corpus._positions(params[0], params.piece, user, possiblePaths, sendData)
  }

  static imagePositions (req, res) {
    printlog(req, "get image positions");
    Corpus._imagePositions(req.params, req.user, (err, data) => {
      respond(res, data, err)
    })
  }

  static _audioImagePositions(params, user, sendData) {
    let possiblePaths = [
      '/sources/audios/' + params.audioName + '/images/' + params.imageName + '/positions.json', // nominal
      '/' + params.imageName + '-pos.json' // ex: bwv847-waves-pos.json
    ]
    Corpus._positions(params[0], params.piece, user, possiblePaths, sendData)
  }

  static audioImagePositions (req, res, next) {
    printlog(req, "get image positions");
    Corpus._audioImagePositions(req.params, req.user, (err, data) => {
      respond(res, data, err)
    })
  }

  static _audioSynchro(params, user, sendData) {
    let possiblePaths = [
      '/sources/audios/' + params.audioName + '/synchro.json', // nominal
      '/synchro.json'
    ]
    Corpus._positions(params[0], params.piece, user, possiblePaths, sendData)
  }

  static audioSynchro (req, res, next) {
    printlog(req, "get audio synchro");
    Corpus._audioSynchro(req.params, req.user, (err, data) => {
      respond(res, data, err)
    })
  }

  static _mediaFile(path, id, user, possiblePaths, sendFile) {
    let root = formatePath(path) + id
    if (!fs.existsSync(root)) {
      sendFile(undefined, "NOT_FOUND")
      return
    }
    // if (!can('read', user, root)) {
    //   sendFile(undefined, "FORBIDDEN")
    //   return
    // }
    for (const filepath of possiblePaths) {
      let fp = root + filepath
      if (fs.existsSync(fp)) {
        sendFile(fp)
        return
      }
    }
    sendFile(undefined, "NOT_FOUND")
  }

  static _imageFile(params, user, sendFile) {
    let possiblePaths = [
      '/sources/images/' + params.imageName + '/' + params.imageName + '.png', // nominal
      '/sources/images/' + params.imageName + '/' + params.imageName + '.jpg', // nominal
      '/' + params.imageName + '.png',
      '/' + params.imageName + '.jpg'
    ]
    Corpus._mediaFile(params[0], params.piece, user, possiblePaths, sendFile)
  }

  static imageFile (req, res, next) {
    printlog(req, "get image file");
    Corpus._imageFile(req.params, req.user, (filepath, err) => {
      if (err) {
        respond(res, undefined, err)
      } else {
        res.sendFile(config.corpusDir + filepath)
      }
    })
  }

  static _audioImageFile(params, user, sendFile) {
    let possiblePaths = [
      '/sources/audios/' + params.audioName + '/images/' + params.imageName + '/' + params.imageName + '.png', // nominal
      '/sources/audios/' + params.audioName + '/images/' + params.imageName + '/' + params.imageName + '.jpg', // nominal
      '/' + params.imageName + '.png',
      '/' + params.imageName + '.jpg'
    ]
    Corpus._mediaFile(params[0], params.piece, user, possiblePaths, sendFile)
  }

  static audioImageFile (req, res) {
    printlog(req, "get image file");
    Corpus._audioImageFile(req.params, req.user, (filepath, err) => {
      if (err) {
        respond(res, undefined, err)
      } else {
        res.sendFile(config.corpusDir + filepath)
      }
    })
  }

  static _audioFile(params, user, sendFile) {
    let possiblePaths = [
      '/sources/audios/' + params.audioName + "/"+ params.audioName + '.mp3', // nominal
      '/' + params.audioName + '.mp3'
    ]
    Corpus._mediaFile(params[0], params.piece, user, possiblePaths, sendFile)
  }

  static audioFile (req, res) {
    printlog(req, "get audio file");
    Corpus._audioFile(req.params, req.user, (filepath, err) => {
      if (err) {
        respond(res, undefined, err)
      } else {
        res.sendFile(config.corpusDir + filepath)
      }
    })
  }

  static _analyses(path, id, user, sendData) {
    let analysesDir = formatePath(path) + id + '/analyses'
    if (!fs.existsSync(analysesDir)) {
      sendData("NOT_FOUND", "")
      return
    }
    let principal = user?user:{"uid": "public", "groups": []}
    if (!can('read', principal, analysesDir)) {
      sendData("FORBIDDEN", "")
      return
    }
    fs.readdir(analysesDir, (err, files) => {
      if (files == undefined) {
        sendData("NOT_FOUND", "")
        return
      }
      let dezFiles = files
      .filter(f => f.endsWith(".dez"))
      .filter(f => can('read', principal, analysesDir, f))
      if (err) {
        sendData(err, dezFiles)
      } else {
        sendData(err, dezFiles)
      }
    });
  }

  static analyses (req, res, next) {
    printlog(req, "get analyses");
    Corpus._analyses(req.params[0], req.params.piece, req.user, (err, data) => {
      respond(res, data, err)
    })
  }

  static _analysis (path, id, name, user, sendData) {
    let analysesDir = formatePath(path) + id + '/analyses/'
    let principal = user?user:{"uid": "public", "groups": []}
    if (!can('read', principal, analysesDir, name)) {
      sendData("FORBIDDEN", "")
      return
    }
    fs.readFile(analysesDir + name, 'utf8', (err, data) => {
      if (err) return
      let analysis = JSON.parse(data)
      if (can('read-access', principal, analysesDir, name)) {
        analysis["access"] = Corpus._getAccessOf(analysesDir, name)
      }
      sendData(err, JSON.stringify(analysis, null, 2))
    })
  }

  static analysis (req, res, next) {
    printlog(req, "get analysis");
    Corpus._analysis(req.params[0], req.params.piece, req.params.name, req.user, (err, data) => {
      respond(res, data, err, true)
    })
  }

  static _createAnalysis (path, id, name, content, user, sendData) {
    if (
      name.includes("reference.dez") ||
      name.includes("computed.dez") ||
      name == "algomus.dez" ||
      name == "empty.dez"
    ) {
      sendData("FORBIDDEN", "Dezrann samples can not be modified")
      return
    }
    if (!can('update-analysis', user, path.slice(1) + '/' + id + '/analyses/', name)) {
      sendData("FORBIDDEN", "No credentials")
      return
    }
    let dir = path.slice(1) + '/' + id + '/analyses/';
    if (fs.readdirSync(dir).length < 15) {
      let fileName = dir + name;
      fs.writeFile(fileName, content, 'utf8', (err, data) => {
        if (err) {
          sendData(err, data)
        } else {
          sendData(err, "file " + fileName + "created");
        }
      })
    } else {
      console.log("number of analysis exceded");
    }
  }

  static createAnalysis (req, res, next) {
    printlog(req, "NEW ANALYSIS -> ");
    Corpus._createAnalysis(req.params[0], req.params.id, req.body.name, req.body.content, req.user, (err, data) => {
        respond(res, data, err)
    })
  }


}

module.exports = Corpus;
