/*
  This file is part of Dezrann <http://www.dezrann.net>
  Copyright (C) 2016-2021 by Algomus Team at CRIStAL (UMR CNRS 9189, Université Lille),
  in collaboration with MIS (UPJV, Amiens) and LITIS (Université de Rouen).

  Dezrann is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Dezrann is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Dezrann.  If not, see <https://www.gnu.org/licenses/>.
*/

let path = require('path');
let http = require('http');
let connect = require('connect');
let io = require('socket.io');
let fs = require('fs');
let Operation = require('./lib/Operation')
let OperationLog = require('./lib/OperationLog')

//change clientdir here
let clientdir = path.join(process.cwd(), '../');
let app = connect().use(connect.static(clientdir));
let server = http.createServer(app);
let origin_version = '02-reference.dez';
let log_file = 'log_file.json';
let broadcast_version = [];

function updateLogAndBroadcast (op, socket) {
	let update_operation = new Operation(op, socket.id);
	logs[op.piece].addOperation(update_operation);
	if (op.action == "LOAD_NEW_ANALYSIS") {
		socket.broadcast.emit('analysis-channel', {
			current_analysis : logs[op.piece].analysisLabels,
			current_version : logs[op.piece].version,
			piece : op.piece
		});
	} else {
		socket.broadcast.emit('change-analysis', {
			current_operation : update_operation
		});
	}
	socket.emit('accept_channel', {
		current_operation : update_operation
	});
}
function commutative (operation, version, socketId) {
	if (operation.action == "LOAD_NEW_ANALYSIS") {
		// if a label was changed by others since version the new analysis can not
		// be loaded
		for (let label of logs[operation.piece].analysisLabels) {
			for (let op of logs[operation.piece].from(version)) {
				if ((op.socketId != socketId) && (label.id == op.op.labelId)) return false
			}
		}
	} else {
		for (let op of logs[operation.piece].from(version)) {
			if ((op.socketId != socketId) && (op.op.labelId == operation.labelId)) {
				for (let key in op.op.changes) {
					if (key != "id" && key in operation.changes) {
						return false
					}
				}
			}
		}
	}
	return true
}

function handleOperation(data, version, socket) {
	if (data.server_version == version) {
		console.log("INFO : update log", data.client_version, data.server_version, version);
		updateLogAndBroadcast(data.op, socket)
	} else if (data.server_version < version) {
		console.log("WARNING : client is late from the server", data.server_version, version);
		if (commutative(data.op, data.server_version, socket.id)) {
			updateLogAndBroadcast(data.op, socket)
		} else {
			socket.emit('deny_channel', {
				last_op : data.op
			})
		}
	} else {
		console.log("ERROR : client has a version number too high !", data.server_version, version);
	}

}

//read log_file
fs.exists(log_file, exists => {
	if(!exists) {
		fs.open(log_file, "w", (err, fd) => {});
	} else {
		fs.unlink(log_file, (err,fd) => {});
	}
})

let socketServer = io.listen(server);
socketServer.set('log level',0);

let logs = {}

socketServer.sockets.on('connection', socket => {
	let ip = socket.handshake.address.address;
	let id = socket.id
	console.log('Socket connection. ' + ip, id);
	//handle discnnect
	socket.on('disconnect', client => {
		console.log('Socket disconnect. ' + ip, id);
	});
	//if new client connect
	socket.on('analysis-demande', data => {
		if (!(data.piece in logs)) {
			logs[data.piece] = new OperationLog()
		}
		analysis=logs[data.piece].generateAnalysisByOperation()
		socket.emit('analysis-channel', {
			current_analysis : analysis.labels,
			current_version : logs[data.piece].version,
			piece : data.piece
		});
	})
	//dez-collab channel
	socket.on('update-label', data => {
		if (logs[data.op.piece].containsLabel(data.op.labelId)) {
			handleOperation(data, logs[data.op.piece].version, socket)
		} else {
			socket.emit('deny_channel', {
				last_op : data.op
			})
		}
	});
	socket.on('remove-label', data => {
		updateLogAndBroadcast(data.op, socket)
	});
	socket.on('create-label', data => {
		updateLogAndBroadcast(data.op, socket)
	});

	socket.on('load-new-analysis', data => {
		if (!(data.op.piece in logs)) {
			logs[data.op.piece] = new OperationLog()
		}
		handleOperation(data, logs[data.op.piece].version, socket)
	})

	// needed for fonctional tests
	socket.on('reinit', () => {
		operationLog = new OperationLog()
	})
});

server.listen(3000, () => {
	console.log('server started on localhost:3000, please visit this adresse');
});
