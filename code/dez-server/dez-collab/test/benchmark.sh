#!/bin/bash
# usage : benchmark [conflict] [nbKeys]
# - conflict: conflict should exists
# - nbKeys : number of attribute of the label
conflict=0
nbKeys=1
echo "nb clients;emitted;accepted;accepted rate;denied;denied rate;missed ack;total time;action time" > /tmp/benchmark.csv;
if [ $1 ]
then
	if [ $1 = "conflict" ]
	then
		conflict=1
		if [ $2 ]
		then
			nbKeys=$2
		fi
	else
		nbKeys=$1
	fi
fi
for i in {1..100}; do
	cd ..; node app.js > /dev/null & cd test;
	sleep 1;
	pid=`ps aux| grep app.js| grep node| awk '{ print $2 }'`;
	out=`node benchmark.js $i $conflict $nbKeys`;
	echo $out;
	echo $out \
		| awk '{print ""$6";"$9";"$13";"$14";"$18";"$19";"$24";"$1";"$2""}'\
		| sed "s/]//"\
		| sed "s/(//g"\
		| sed "s/)//g" \
		| sed "s/%//g"\
		| sed "s/\./,/g" >> /tmp/benchmark.csv;
	kill -TERM $pid > /dev/null;
done
