Prérequis python:

- python3
- dépendances : music21, matplotlib, numpy, scipy et pydub :

`pip3 install matplotlib numpy scipy pydub`

Procédure 2018 pour ajouter un corpus

  - modifier /code/corpus/corpus.py
        C’est mieux de sous-classer pour avoir de plus jolies méta-données
        dépendant du corpus (et aussi de jolis identifiants type bwv847).
 	Néanmoins, avec GenericCorpus on peut désormais avoir des identifiants
        corpus-001 ou corpus-nom-du-fichier.

  - lancer corpus.py (qui dépend de music21 actuel, non pas du notre)

  - le corpus.py modifié peut être mis dans dev… (en commentant c.process()
    si on ne veut pas que cela apparaisse dans le public à une prochaine mise à jour)

  - on peut rajouter "hide": true dans le info.json si on veut cacher ces pièces
    (voir stand-by-me/info.json dans prod-internal)

  - vérifier ce que cela donne sur votre Dezrann localhost (attention d’avoir bien un index.html
    qui pointe vers localhost et non pas vers les serveurs…)

  - puis rajouter tous les fichiers /corpus/* dans le git, soit dans prod-internal, soit dans dev,
    selon où on veut qu’apparaissent les fichiers

  - et voir avec Manu pour remettre ces fichiers dans sonates.lifl.fr ou dezrann.lifl.fr selon les cas.


Pour ajouter un seul fichier, on peut ne pas modifier corpus.py et directement appeler d'autres scripts,
puis remettre les /corpus/* dans le git... mais ce n'est pas très reproductible par la suite.
À ne faire que quand on fait des tests sur des nouvelles vues / générations (type stand-by-me).

=======

Pour ajouter un .mp3 edmus (sur branche edmus)

  - créer un répertoire /code/corpus/data/edmus/id-piece/, mettre id-piece.mp3 et d'autres choses sources, committer l'ensemble

  - depuis code/corpus, lancer
      python3 piece.py data/edmus/id-piece/id-piece.mp3
    => création et remplissage de /corpus/id-piece/...
  - dans synchro.json *et* id-piece-waves-pos.json, remplacer le 99 de "date" par la durée en secondes (moins quelques secondes ?) #644

  - si besoin, éditer aussi les positions des staves pour que ce soit joli ?

  - si lyrics : mettre id-piece.lyrics, et appeler
      python3 lyrics.py id-piece
  - puis éditer info.json à la main pour rajouter ce qu'il faut
  (voir stand-by-me/info.json)  #645. En particulier la propriété `hide` à
  positionner à `true` dans le cas d'edmus.

  - committer le répertoire résultant (y compris les .png et .mp3...)
