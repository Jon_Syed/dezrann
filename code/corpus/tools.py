
import json

def write_json(j, f):
    print('==> %s' % f)
    ff = open(f, 'w')
    ff.write(json.dumps(j, sort_keys=True, indent=4))
    ff.close()

