import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

var globalVariable = globalVariable || {}

class DezGlobalVariable extends PolymerElement {
  static get properties() {
    return {
      key: String,
      value: {
        type: Object,
        notify: true,
        observer: '_valueChanged'
      },
      readonly: {
        type: Boolean,
        value: false,
      }
    }
  }

  connectedCallback () {
    if (this.key) {
      if (globalVariable[this.key]) {
        if (this.value != globalVariable[this.key].value) {
          this.value = globalVariable[this.key].value;
        }
      } else {
        globalVariable[this.key] = {
          value: this.value,
          observers: [this],
          isChanging: false,
        };
      }
      this._setObserver(this.key);
    } else {
      throw new Error('Invalid Argument: The `key` property must be defined.');
    }
  }

  disconnectedCallback () {
    var index = globalVariable[this.key].observers.indexOf(this);
    if (index !== -1) {
      globalVariable[this.key].observers.splice(index, 1);
    }
  }

  _setObserver (key) {
    var index = globalVariable[key].observers.indexOf(this);
    if (index === -1) {
      globalVariable[key].observers.push(this);
    }
  }

  _valueChanged (value, oldValue) {
    if (!this.readonly && value != oldValue &&
      globalVariable[this.key] &&
      value != globalVariable[this.key].value &&
      !globalVariable[this.key].isChanging) {
      this._setValue(this.key, value);
    }
  }

  _setValue (key, value) {
    globalVariable[key].isChanging = true;
    globalVariable[key].value = value;
    for (var i in globalVariable[key].observers) {
      if (globalVariable[key].observers[i] != this) {
        globalVariable[key].observers[i].set('value', value);
      }
    }
    globalVariable[key].isChanging = false;
  }

}

window.customElements.define('dez-global-variable', DezGlobalVariable);
