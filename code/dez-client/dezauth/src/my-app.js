/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-localstorage/iron-localstorage.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-meta/iron-meta.js';
import './dez-global-variable.js';
import './dez-logout.js';
import './dez-breadcrumbs.js';
import './my-icons.js';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(MyAppGlobals.rootPath);

class MyApp extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          --app-primary-color: #4285f4;
          --app-secondary-color: black;

          display: block;
        }

        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }

        app-header {
          color: #fff;
          background-color: var(--app-primary-color);
        }

        app-header paper-icon-button {
          --paper-icon-button-ink-color: white;
        }

        .drawer-list {
          margin: 0 20px;
        }

        .drawer-list a {
          display: block;
          padding: 0 16px;
          text-decoration: none;
          color: var(--app-secondary-color);
          line-height: 40px;
        }

        .drawer-list a.iron-selected {
          color: black;
          font-weight: bold;
        }

        .circle {
          display: inline-block;
          width: 32px;
          height: 32px;
          text-align: center;
          color: #555;
          border-radius: 50%;
          background: #ddd;
          font-size: 20px;
          line-height: 32px;
        }

        app-toolbar a {
          text-decoration: none;
        }

      </style>

      <iron-ajax auto url="/config.json" handle-as="json" last-response="{{config}}"></iron-ajax>
      <iron-meta id="config" key="config" value$="{{config}}"></iron-meta>

      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>

      <app-drawer-layout fullbleed="" narrow="{{narrow}}">
        <!-- Drawer content -->
        <!--
        <app-drawer id="drawer" slot="drawer" swipe-open="[[narrow]]">
          <app-toolbar>Menu</app-toolbar>
          <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
            <a name="corpus" href="[[rootPath]]corpus">View One</a>
            <a name="view2" href="[[rootPath]]view2">View Two</a>
            <a name="view3" href="[[rootPath]]view3">View Three</a>
          </iron-selector>
        </app-drawer>
        -->

        <!-- Main content -->
        <app-header-layout has-scrolling-region="">

          <app-header slot="header" condenses="" reveals="" effects="waterfall">
            <app-toolbar>
              <paper-icon-button icon="my-icons:menu" drawer-toggle=""></paper-icon-button>
              <div main-title="">Dezauth</div>
              <a name="login" href="[[rootPath]]login" hidden$="[[storedUser.loggedin]]">Log in</a>
              <div hidden$="[[!storedUser.loggedin]]">
                <a name="login" href="[[rootPath]]changepw">
                  <div class="circle">[[storedUser.name.0]]</div>
                </a>
                <dez-logout stored-user ="{{storedUser}}" link></dez-logout>
              </div>
            </app-toolbar>
          </app-header>

          <dez-breadcrumbs page=[[routeData.page]] path=[[subroute.path]]></dez-breadcrumbs>

          <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
            <dez-corpus-view name="corpus" stored-user="[[storedUser]]"></dez-corpus-view>
            <dez-piece-view name="piece" stored-user="[[storedUser]]"></dez-piece-view>
            <dez-login name="login"></dez-login>
            <dez-change-password name="changepw"></dez-change-password>
            <my-view404 name="view404"></my-view404>
          </iron-pages>
        </app-header-layout>
      </app-drawer-layout>

      <iron-localstorage name="user-storage" value="{{storedUser}}"></iron-localstorage>
      <dez-global-variable key="userData" value="{{storedUser}}"></dez-global-variable>

    `;
  }

  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object,
      storedUser: Object
    };
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
     // Show the corresponding page according to the route.
     //
     // If no page was found in the route data, page will be an empty string.
     // Show 'corpus' in that case. And if the page doesn't exist, show 'view404'.
    if (!page) {
      this.page = 'corpus';
    } else if (['corpus', 'piece', 'login', 'changepw'].indexOf(page) !== -1) {
      this.page = page;
    } else {
      this.page = 'view404';
    }

    // Close a non-persistent drawer when the page & route are changed.
    // if (!this.$.drawer.persistent) {
    //   this.$.drawer.close();
    // }
  }

  _pageChanged(page) {
    // Import the page component on demand.
    //
    // Note: `polymer build` doesn't like string concatenation in the import
    // statement, so break it up.
    switch (page) {
      case 'corpus':
        import('./dez-corpus-view.js');
        break;
      case 'piece':
        import('./dez-piece-view.js');
        break;
      case 'login':
        import('./dez-login.js');
        break;
      case 'changepw':
        import('./dez-change-password.js');
        break;
      case 'view404':
        import('./my-view404.js');
        break;
    }
  }
}

window.customElements.define('my-app', MyApp);
