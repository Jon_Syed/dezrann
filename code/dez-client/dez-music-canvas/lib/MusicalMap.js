function round2(x) {
    return Math.round(x * 100) / 100;
}

function findDicho(element, slice, from, to) {
    let first = 0;
    let last = slice.length - 1;
    if (element <= slice[first][from]) {
        return slice[first][to]
    }
    if (element >= slice[last][from]) {
        return slice[last][to]
    }
    let index = Math.round((last-first)/2)
    let current = slice[index]
    let previous = slice[index-1]
    if (previous[from] <= element && element <= current[from]) {
        let alpha = (element - previous[from]) / (current[from] - previous[from])
        return round2(previous[to] + (current[to] - previous[to]) * alpha)
    } else if (element > current[from]) {
        return findDicho(element, slice.slice(index, last+1), from, to)
    } else if (element < previous[from]) {
        return findDicho(element, slice.slice(first, index), from, to)
    }
}


class MusicalMap {
    constructor () {
        this._positions = [];
        this._timeByMeasure = 4
        this._divByTime = 4
        this._lastOffset = undefined
    }

    set positions (positions) {
      this._positions = positions;
      let lastOnset = this._positions[this._positions.length - 1].onset
      if (this._lastOffset && this._lastOffset <= lastOnset) {
        this._lastOffset = undefined
      }
    }

    set lastOffset (value) {
      if (value == undefined) return
      let lastOnset = this._positions[this._positions.length - 1].onset
      if (value > lastOnset) {
        this._lastOffset = value
      }
    }

    toMusicalOnset (x) {
        return findDicho(
            x,
            this._positions,
            "x",
            "onset"
        )
    }
    previousOnset (onset) {
      let lastOnset = this._positions[this._positions.length - 1].onset
      let firstOnset = this._positions[0].onset
      if (onset > lastOnset) {
        return lastOnset
      } else if (onset <= firstOnset ) {
        return firstOnset
      }
      let previous
      for (let position of this._positions) {
        if (position.onset >= onset) return previous
        previous = position.onset
      }
    }

    offset (onset) {
      let lastOnset = this._positions[this._positions.length - 1].onset
      let firstOnset = this._positions[0].onset
      let o = onset
      if (onset >= lastOnset) {
        if (this._lastOffset) return this._lastOffset
        return lastOnset
      } else if (o < firstOnset) {
        o = firstOnset
      }
      let i = 0
      while (this._positions[i].onset < o && i < this._positions.length - 1) {
        i++
      }
      if (i < this._positions.length - 1 && this._positions[i].onset <= o) {
        return this._positions[i+1].onset
      }
      return this._positions[i].onset
    }

    toMusicalDuration (startx, length, ioiMode = true) {
      if (length < 0) return undefined
      let start = this.toMusicalOnset(startx)
      let last = this.toMusicalOnset(startx+length)
      if (!ioiMode) return this.offset(last) - start
      return last - start
    }
    toGraphicalX(onset) {
        let x = findDicho(
            onset,
            this._positions,
            "onset",
            "x"
        )
        return x;
    }
    toGraphicalLength (start, duration, ioiMode = true) {
      if (duration < 0) return undefined
      let begin = this.toGraphicalX(start);
      let previous = this.previousOnset(start+duration);
      let end = this.toGraphicalX(ioiMode?start+duration:previous);
      return end>begin?end-begin:0
    }

}

class WaveMap extends MusicalMap {

   constructor (synchro) {
        super();
        this._positions = [];
        this._synchro = synchro
        this._timeByMeasure = 4
        this._divByTime = 4
    }

    set positions (value) {
      this._positions = value
      let pos = []
      for (const item of this._synchro) {
        pos.push({
          onset : item.onset,
          date : item.date,
          x : this.toGraphicalX(item.onset)
        })
      }
      this._positions = pos
    }

    toMusicalOnset (x) {
      return this._synchro.toOnset(
        findDicho(
          x,
          this._positions,
          "x",
          "date"
        )
      )
    }

    toGraphicalX(onset) {
      return findDicho(
        this._synchro.toDate(onset),
        this._positions,
        "date",
        "x",
      )
    }

    regularGraphicalGrid (step, upbeat) {
        let ret = []
        for (let i = upbeat; i < this._synchro.last.onset; i+=step) {
            ret.push(this.toGraphicalX(i));
        }
        return ret;
    }


}

class Synchro {

  constructor (synchroData) {
    this._synchroData = synchroData
  }

  get last () {
    return this._synchroData.slice(-1)[0]
  }

  *_iterator() {
    for (const value of this._synchroData) {
      yield value;
    }
  }

  [Symbol.iterator]() {
    return this._iterator();
  }

  toOnset (date) {
    return findDicho(
        date,
        this._synchroData,
        "date",
        "onset"
    )
  }

  toDate (onset) {
    return findDicho(
      onset,
      this._synchroData,
      "onset",
      "date"
    )
  }

}
