class Label {

  constructor (jsonLabel) {
    this.id = jsonLabel.id
    this.type = jsonLabel.type
    this.tag = jsonLabel.tag
    this.comment = jsonLabel.comment
    this.start = jsonLabel.start
    this.duration = jsonLabel["actual-duration"]
    this.ioi = jsonLabel.duration
    this.staff = jsonLabel.staff
    this.line = jsonLabel.line
    this.color = jsonLabel.color
    this.layers = jsonLabel.layers
  }

  get end () { return this.start + this.genericDuration; }
  set end (end) {
    if (end >= this.start) {
      this.genericDuration = end - this.start
    }
  }

  get ioiMode () { // true by default
    return this.duration == undefined
  }

  get genericDuration () {
    if (this.ioi == undefined && this.duration == undefined) return 0
    return this.ioiMode?this.ioi:this.duration
  }

  set genericDuration (value) {
    if (value == 0) return
    if (this.ioiMode) {
      this.ioi = value
    } else {
      this.duration = value
    }
  }

  toString() {
    let s = ''
    s += this.id + '-(' + this.type + ','
    s += this.start
    // s += toMeasureFrac(this.start, this.timeSigNum, this.timeSigDenum, this.timeSigUpbeat) + '@' + this.start.toFixed(2) + ','
    s += this.tag + ')'
    return s
  }

}
