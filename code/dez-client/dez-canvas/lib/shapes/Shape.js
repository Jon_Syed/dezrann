class Shape {

    constructor (view, resizers, defaults) {
        this._view = view;
        this._OPACITY = 0.2;
        if (defaults && defaults["color"]) {
            this._color = defaults["color"];
        }
        this._textPosition = "inside"
        this._textStyle = 'grob-text'
        this.MIN_WIDTH = 20;
    }

    get text () {
        return (this._text == undefined) ? '' : this._text.text;
    }
    set text (text) {
        if (this._text == undefined) {
            this._text =  this._view.createText(text, this._textStyle)
        } else {
            this._text.text = text
        }
    }

    set color (color) { this._color = color; }

    remove () {
        if (this._text != undefined) {
            this._text.remove()
        }
    }

    showAsSelected () {
        console.log("Shape.showAsSelected() has to be implemented");
    }

    showAsUnselected () {
        console.log("Shape.showAsUnselected() has to be implemented");
    }

}
