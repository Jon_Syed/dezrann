class SnapTriangle {

    constructor (polygon, x, y, width, height, direction, marginTop) {
        this._snapPolygon = polygon;
        this._marginTop = marginTop

        this._pos_x = undefined;
        this._pos_y = undefined;
        this._width = undefined;
        this._height = undefined;

        this.direction = direction;

        this.redraw(x, y, width, height, direction);
    }

    get _leftPoint ()  {
        let posX = this._pos_x      - this.width / 2;
        let posY = undefined;
        if (this.direction == "up")
            posY = this._pos_y      + this._marginTop + this.height ;
        else
            posY = this._pos_y      + this._marginTop ;
        return {x: posX,   y: posY};
    }
    get _rightPoint () { return {x: this._leftPoint.x + this.width,      y: this._leftPoint.y}; }
    get _spikePoint () {
        let posX = this._leftPoint.x + this.width / 2;
        let posY = undefined;
        if (this.direction == "up")
            posY = this._pos_y      + this._marginTop + this.height / 2 ;
        else
            posY = this._leftPoint.y + this.height / 2 ;
        return {x: posX,  y: posY};
    }

    get width () { return this._width; }
    get height () { return this._height; }

    get x () { return this._pos_x; }
    get y () { return this._pos_y; }

    set direction (direction) { this._direction = direction; }
    get direction () { return this._direction; }

    _updateAttr() {
        let tab = [
            this._leftPoint.x,  this._leftPoint.y,
            this._rightPoint.x, this._rightPoint.y,
            this._spikePoint.x, this._spikePoint.y
        ];
        this._snapPolygon.attr({"points": tab});
    }

    redraw (x, y, width, height) {
        this._pos_x = x ;
        this._pos_y = y ;
        if (width >= 10)
            this._width = width;
        else
            this._width = 10;
        this._height = height;
        this._updateAttr();
    }

    canContain (box, marginWidth, marginHeight) {
        return this.width > box.width + marginWidth
            && this.height > box.height + marginHeight
    }

    remove () {
        this._snapPolygon.remove();
    }

    set strokeWidth (value) {
        this._snapPolygon.attr({strokeWidth: value})
    }

    set opacity (value) {
        this._snapPolygon.attr({opacity: value})
    }
    contains (x) {
        if (this._snapPolygon == undefined) return false;
        return this._leftPoint.x <= x && x <= this._rightPoint.x;
    }

}
