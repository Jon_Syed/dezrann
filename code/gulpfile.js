const gulp = require('gulp'),
  rmLines = require('gulp-rm-lines');

gulp.task('remove-CommonJS', function () {
  gulp.src('./lib/collab/*.js')
    .pipe(gulp.dest('dez-server/dez-collab/test/lib'))
    .pipe(rmLines({
      'filters': [
        /.*=[ ]*require[ ]*\([ ]*\'.*\'[ ]*\)/i,
        /[ ]*module\.exports[ ]*=[ ]*.*/i
      ]
    }))
    .pipe(gulp.dest('dez-client/dez-collab/lib'));
});

gulp.task('default', [ 'remove-CommonJS'], function() {
  gulp.watch('./lib/collab/*.js', ['remove-CommonJS']);
});
