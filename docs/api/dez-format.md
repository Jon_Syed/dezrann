

# Encoding music annotations with `.dez` files

Dezrann uses [json](http://en.wikipedia.org/wiki/JSON) files to encode an "analysis", that is a set of **labels** describing music annotations.
It is intended to be very small (at most a few kilobytes) and refers to the music notation, being independent of any audio file.

Here is such a `.dez` file:

```json
{  
  "labels": [
    { "type": "S", "start": 8.5, "duration": 7.5, "staff": "1"},
    { "type": "CS1", "start": 8.25, "duration": 7.75, "staff": "2"},
    { "type": "S", "start": 0.5, "duration": 7.5, "staff": "2"},
  
    { "start": 8, "tag": "Second voice enters"},    
    { "start": 16 },  
    { "type": "Pattern", "start": 16.5, "duration": 1.5, "tag": "h", "layers": ["predicted"]},
    
    { "type": "Structure", "start": 16, "duration": 8, "tag": "codetta"},        
    { "type": "Pedal", "start": 114, "actual-duration": 10, "tag": "Tonic pedal"},
    { "type": "Cadence", "start": 64, "tag": "v:PAC"}
  ],
  
  "meta":{
    "date": "2021-01-06T13:50:38.306Z",
    "producer": "Dezrann c6c80150",
    "title": "Fugue in C minor",
    
    "layout": [
      { "filter" : { "type": "Structure" }, "style" : {"line": "bot.1"} },
      { "filter" : { "type": "Pedal" }, "style" : {"line": "top.1", "color": "#bbbbbb"} },
      { "filter" : { "type": "Pattern", "layers": ["predicted"] }, "style" : {"line": "top.2"} }
    ]
  }
}
```

The analysis [02-reference.dez](http://dezws.lifl.fr/corpus/bwv847/analyses/02-reference.dez) shows a more complete example.


## `labels`: list of annotations

The `labels` list is the only mandatory part of a `.dez` file.
Each label may have the following attributes:

- `start` *(musical time, float, mandatory)*: This is the only mandatory field.
- `duration` *(musical time, float, default 0)*: inter-offset-interval (IOI), that is duration from the start of the first note to the *start* of the last note
- `actual-duration` *(musical time, float, default 0)*: actual duration, that is duration from the start of the first note to the *end* of the last note
   <!-- To be detailed -->

A label with no `duration` neither `actual-duration` is supposed be "instant" (and will be displayed without any width).
The `actual-duration` is closer to what people usually mean by "duration", but the `duration` (IOI) allows better comparison between similar labels.
A label can have both a `duration` and an `actual-duration`.
The conversion between the two is not always straightforward, in particular in a polyphonic context.

- `staff` *(integer, default 0)*: staff concerned by the label, 1 being the the staff on the top. If this is 0, the label concerns all the score

- `type` *(string, default "")*: base type of the label, from a controled vocabulary. See <user/label-types.md> for the default types.
- `tag` *(string, default "")*: further details on the labels, depending on the type. This should again be somewhat controlled according to each application.
- `comment` *(string, default "")*: comment, in free text. This can have several lines.

- `layers` *(string list, default "")*: annotation sources, such as `["tovey"]` or `["predicted, algorithm5"]`

Note that a label can also have graphic attributes (such as `line`), but this is generally not recommended:
One should use instead the `meta.layout` block as described below.

Thus the `labels` list should be as independent as possible from any actual implementation of Dezrann or any other visualisation platform.
It can be exported from `music21` with our `music21.schema` package.


### Musical time

`start`, `duration` and `actual-duration` are expressed in *quarter notes* from the beginning of the score.
TODO: Explain upbeat.

In Dezrann, they are displayed, by default, in measure/beat/offset.
TODO: Can be encoded as measure/beat/offset in .dez ?



## `meta`: analysis and piece metadata

The `meta` block list metadata attributes of the analysis and possibly of the piece (#899).

## `meta.layout`: label formatting options

The `meta.layout` block lists rules defining styling options for the labels,
as in `{ "filter" : { "type": "Pedal" }, "style" : {"line": "top.1", "color": "#bbbbbb"} }`.

Each rule has

- a `filter` attribute, now supporting `type` and `layers` queries. #938
- a `"style"` attribute, defining styling and formatting options. The currently supported option is:
    * `line`: By default, out-staff labels are displayed on six lines: `top.1`, `top.2`, `top.3` `bot.1`, `bot.2`, `bot.3`
    * `color` will be supported #937

# References

- M. Giraud, R. Groult, E. Leguy. [Dezrann, a Web Framework to Share Music Analysis](https://hal.archives-ouvertes.fr/hal-01796787). International Conference on Technologies for Music Notation and Representation (TENOR 2018), pp.104-110.





