


### Credits, references

Dezrann is developped by Emmanuel Leguy and the [Algomus](http://www.algomus.fr)> computer music team.
Dezrann is open-source (GPL v3+). 
The code and the public issue tracker are on <http://www.gitlab.com/algomus.fr/dezrann/>.
Dezrann has been tested on recent Firefox, Safari and Chrome web browsers.

### References

- Mathieu Giraud, Richard Groult, Emmanuel Leguy,
  [Dezrann, a Web Framework to Share Music Analysis](https://hal.archives-ouvertes.fr/hal-01796787),  TENOR 2018, pp. 104-110.

- Ming La, Mathieu Giraud, Emmanuel Leguy,
  [Realtime collaborative annotation of music scores with Dezrann](https://hal.archives-ouvertes.fr/hal-02162935>, CMMR 2019.
  
- 🇫🇷 [Favoriser l'écoute autonome en éducation musicale](https://www.ac-amiens.fr/2512-favoriser-l-ecoute-autonome-en-education-musicale.html), Académie d'Amiens, 2019.
  
      
