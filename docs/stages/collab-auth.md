# Plateforme collaborative temps réel d'analyse de partitions de musique

## Contexte

L'équipe Algomus développe [Dezrann](http://www.dezrann.net), une application
web pour lire et annoter des partitions musicales. Cette application utilise le
framework [Polymer](http://polymer-project.org) qui implémente la technologie
des *Web Components* (futur standard du W3C). Nous réalisons un ensemble de
balises HTML paramétrables qui s'intègrent aisément dans une page web à la
manière des balises HTML5 vidéo ou audio. Dezrann est utilisé d'un côté par des
classes de collèges pour découvrir la musique, et de l'autre, par des
musicologues annotant des corpus. Pour que ces utilisateurs puissent travailler
ensemble efficacement nous venons d'ajouter la possibilité de les faire
collaborer en temps réel, à la manière d'un Google doc. Ainsi, cette nouvelle
fonctionnalité permet d'annoter à plusieurs simultanément une partition de
musique.

## Objectifs

L'objectif du projet est de permettre à un utilisateur de *prendre le contrôle*
de la session des autres participants pour leur montrer un endroit particulier
de la partition. Cela sera très utile dans un contexte de classe où le
professeur veut montrer quelque chose à ses élèves, ou quand des élèves veulent interagir entre eux, ou encore quand des musiciens veulent échanger à distance des commentaires sur une partition.

Techniquement, le travail consistera en trois points:
 - Se familiariser avec les technologies utilisées: polymer, socket.io, jwt.
 - Proposer et implémenter un protocole de communication et une UI adhoc.
 - S'interfacer avec le système d'authentification et d'autorisation. Proposer
et implémenter les améliorations nécessaires pour la gestion de salons de
discussion.

Le code sera programmé avec grand soin, documenté et testé.
En cas de succès, ces développements seront testés et déployés avec nos
utilisateurs, notamment en classes de musique avec nos collèges partenaires.


*Mots clés: partitions musicales et analyse, socket.io, json web tokens, UI,
Polymer, Web components, javascript.*


## Liens
- https://socket.io
- http://www.dezrann.net
- http://polymer-project.org

## Bibliographie
- M. Giraud, R. Groult, E. Leguy, [Dezrann, a Web Framework to Share Music Analysis](https://hal.archives-ouvertes.fr/hal-01796787), TENOR 2018
- L. Ma, M. Giraud, E. Leguy, [Realtime collaborative annotation of music scores with Dezrann](https://hal.archives-ouvertes.fr/hal-02162935), CMMR 2019
