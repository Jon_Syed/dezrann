# De la musique plein les yeux.

L'équipe Algomus développe l'application web 'Dezrann' pour lire et annoter
des partitions musicales (prototype sur http://dezrann.net/). L'annotation se fait en ajoutant
des éléments graphiques sur la partition: les 'labels'. Cette application utilise
le framework Polymer qui implémente la technologie des *Web Components*
(standard du W3C). Nous réalisons un ensemble de balises
HTML paramétrables qui s'intègrent aisément dans une page web à la manière des
balises HTML5 vidéo ou audio.

L'objectif du projet est de développer un composant 'éditeur de styles' qui gère
un catalogue de labels. Chaque label a d'abord une sémantique, soit générale (accords,
cadences, modulation...), soit plus spécifique à une musique analysée.
Chaque label possède aussi une représentation graphique (forme, couleur).
Le composant catalogue aura deux types d'accès: administrateur et simple utilisateur.
L'administrateur pourra définir de nouvelles catégories de
labels qui seront accessibles à tous. Un utilisateur pourra marquer les
annotations qui lui sont les plus utiles (favorites) et en inventer d'autres.

Techniquement, le travail consistera à:
1- définir la structure du catalogue,
2- développer un composant Polymer d'administration du catalogue,
3- intégrer le catalogue dans Dezrann,
4- développer un composant de gestion des favoris,
5- développer un composant de création de labels pour tout utilisateur.

Mots clés :  partitions musicales, Polymer, javascript.

Liens :
 - http://dezrann.net/
 - https://www.polymer-project.org/
